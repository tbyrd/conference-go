import requests
from .keys import OPEN_WEATHER_API_KEY


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=limit&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {"lat": resp.json()[0]["lat"], "lon": resp.json()[0]["lon"]}


def get_weather(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)

    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"],
    }
